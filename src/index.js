import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/rootContainer';
import { Provider } from "react-redux";
import {browserHistory, Router, Route } from 'react-router'
import store from './store';
import { syncHistoryWithStore } from 'react-router-redux'

const history = syncHistoryWithStore(browserHistory, store)

const supportsHistory = 'pushState' in window.history;


ReactDOM.render(
  <Provider store={store}>
    { /* Tell the Router to use our enhanced history */ }
    <Router history={history}>
      <Route path="/" component={App}>
        <Route path="/:id" component={App}/>
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)


// ReactDOM.render(
//   <Provider store={store}>
//     <BrowserRouter basename="/" forceRefrech={!supportsHistory}>      
//       <Switch>
//       <Route path="/" component={App}/> 
//       <Route path="/:id" 
//       render={ ({match}) => {
//       	console.log(match.params.id);
//       	return (<div>sdgdg</div>);
//         }
//       }
//     />
//     </Switch>
//     </BrowserRouter>
//   </Provider>,
//   document.getElementById('root')
// );
