import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route, Link, NavLink } from 'react-router-dom';
import Spinner from './Spinner.svg';

class App extends Component {
  componentDidMount(){
    let { getUsers } = this.props;
    getUsers();
  }
  render() {
    let { loaded, data } = this.props.users;
    let baseImgUrl = "https://image.tmdb.org/t/p/";
    let imgSize = "w300"

    if( loaded === true ){
      return (
        <div className="App">  
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React{data.page}</h1>
          </header>  

          {
          data.results.map( (item, key) => {
            let num = "/"+item.id;  
            let img = "".concat(baseImgUrl, imgSize, item.poster_path)     
            return(                        
              <div className="movie-item" key={key}>                
                <NavLink to={num} exact activeClassName="active" >
                  <span className="movie-item-name">{item.title}</span>
                  <img src={img} alt={item.title}/>
                </NavLink>
              </div>              
            );
          })
          }
        </div>
      );
    } else {
      return(<div className="wv"><img src={Spinner}/></div>);
    }
  }
}

export default App;
