import {
  USERS_REQUEST,
  USERS_RESPONSE,
  USERS_ERROR
} from '../constants';
import axios from 'axios';

const baseUrl = 'https://api.themoviedb.org/3/';
const APIKEY = 'ecc8407e38cf623d4ad840c9d7ae7e6a';
const keyWord = 'day';
const myUrl = ''.concat(baseUrl, 'search/movie?api_key=', APIKEY, '&query=', keyWord);
const urlll = "".concat(baseUrl, 'configuration?api_key=', APIKEY); 

const userActionCreator = {
  getUsers: () => {
    return function (dispatch, getState) {
 
      dispatch({
        type: USERS_REQUEST
      });
      axios.get(urlll).then(function(response) {
        console.log(response.data);
      })
      axios.get(myUrl)
      .then(function(response) {
          console.log(response.data);
          console.log(response.data.results);         
          dispatch({
            type: USERS_RESPONSE,
            data: response.data
          });
      });
    };
  }
};

export default userActionCreator;
